package com.aeromexico.trips.notification.constants;

/**
 * The email status enum.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public enum EmailStatusEnum {
	ACTIVE,
	NOT_ACTIVE;
}
