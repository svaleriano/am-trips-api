package com.aeromexico.trips.notification.routes;

import static com.aeromexico.trips.notification.constants.PayloadFormatsEnum.XML;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

import com.aeromexico.trips.notification.processors.NotificationErrorProcessor;
import com.aeromexico.trips.notification.processors.WSErrorProcessor;

/**
 * Notification Routes
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class NotificationRouteBuilder extends RouteBuilder  {
	/** Notification API Base Path */
	private static final String BASE_PATH = "/notifications";
	/** Send Notification Path */
	private static final String SEND_PATH = "/send";
	
	/** Processor to manage errors. */
	private static final WSErrorProcessor errorProcessor =
			new NotificationErrorProcessor();
	
	@Override
	public void configure()
	throws Exception {
        rest(BASE_PATH).description("AM Trip Central - Notification Services")
        
        	.post(SEND_PATH).description("Send Notification Service")
        	.consumes(XML.getContentType())
        	.to("direct:sendNotification");
        
        configureSendNotificationRoute();
	}
	
	/**
	 * Method to configure add device route.
	 */
	private void configureSendNotificationRoute() { // TODO: Add beans resolution.
		from("direct:sendNotification")
		.onException(Exception.class).handled(true).process(errorProcessor).end()
			
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201));
		
		from("direct:send2Email")
		.onException(Exception.class).handled(true).process(errorProcessor).end();
		
		from("direct:send2Device")
		.onException(Exception.class).handled(true).process(errorProcessor).end();
	}
}
