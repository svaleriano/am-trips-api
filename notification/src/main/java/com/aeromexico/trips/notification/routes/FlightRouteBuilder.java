package com.aeromexico.trips.notification.routes;

import static com.aeromexico.trips.notification.constants.NotificationConstants.ADD_FLIGHT_RQ_SCHEMA_FILE;
import static com.aeromexico.trips.notification.constants.NotificationConstants.SCHEMA_FILE_KEY;
import static com.aeromexico.trips.notification.constants.PayloadFormatsEnum.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestParamType;

import com.aeromexico.trips.notification.processors.FlightErrorProcessor;
import com.aeromexico.trips.notification.processors.SchemaValidationProcessor;
import com.aeromexico.trips.notification.processors.WSErrorProcessor;

/**
 * Flight Routes
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class FlightRouteBuilder extends RouteBuilder  {
	/** Flight API Base Path */
    private static final String BASE_PATH= "/flights";
    /** Add Flight Path **/
    private static final String ADD_PATH= "/add";
    /** Flight List Path **/
    private static final String LIST_PATH= "${deviceId}/list";
    /** Flight Detail Base Path */
    private static final String DETAIL_PATH= "${flightNumber}/detail";
    /** Flight Merge Path **/
    private static final String MERGE_PATH= "${deviceId}/merge";
    
    /** Processor to validate json schema. */
    private static final SchemaValidationProcessor schemaValidationProcessor= 
            new SchemaValidationProcessor();
    /** Processor to manage errors. */
    private static final WSErrorProcessor errorProcessor =
    		new FlightErrorProcessor();
    
    @Override
    public void configure() throws Exception {
    	rest(BASE_PATH).description("AM Trip Central - Flight Services")
        
            .post(ADD_PATH).description("Add Flight Service")
        	.param().name("ffToken").type(RestParamType.header).description("The frequent flyer token").required(false).endParam()
        	.param().name("ffNumber").type(RestParamType.header).description("The frequent flyer number").required(false).endParam()
        	.consumes(JSON.getContentType())
        	.to("direct:addFlight")
                
            .get(LIST_PATH).description("Flight List By Device Id Service")
            .param().name("deviceId").type(RestParamType.path).description("The device identifier").required(true).endParam()
            .param().name("ffToken").type(RestParamType.header).description("The frequent flyer token").required(false).endParam()
        	.param().name("ffNumber").type(RestParamType.header).description("The frequent flyer number").required(false).endParam()
            .produces(JSON.getContentType())
            .to("direct:flightList")

	    	.get(DETAIL_PATH).description("Flight Detail Service")
	        .param().name("flightNumber").type(RestParamType.path).description("The flight number").required(true).endParam()
	        .produces(JSON.getContentType())
	        .to("direct:flightDetail")

	    	.put(MERGE_PATH).description("Merge Flights By Device Id Service")
	    	.param().name("deviceId").type(RestParamType.path).description("The device identifier").required(true).endParam()
	    	.param().name("ffToken").type(RestParamType.header).description("The frequent flyer token").required(true).endParam()
	    	.param().name("ffNumber").type(RestParamType.header).description("The frequent flyer number").required(true).endParam()
	    	.consumes(JSON.getContentType())
	    	.to("direct:mergeFlight");
        
    	configureAddFlightRoute();
        configureFlightListRoute();
        configureFlightDetailRoute();
        configureMergeFlightRoute();
    }

    /**
     * Method to configure add flight route
     */
    private void configureAddFlightRoute() {
        from("direct:addFlight")
        .onException(Exception.class).handled(true).process(errorProcessor).end()
	        .setHeader(SCHEMA_FILE_KEY, constant(ADD_FLIGHT_RQ_SCHEMA_FILE))
			.process(schemaValidationProcessor)

            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201));
    }
    
    /**
     * Method to configure flight list route
     */
    private void configureFlightListRoute() {
        from("direct:flightList")
        .onException(Exception.class).handled(true).process(errorProcessor).end()

            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));
    }
    
    /**
     * Method to configure flight detail route
     */
    private void configureFlightDetailRoute() {
    	from("direct:flightDetail")
        .onException(Exception.class).handled(true).process(errorProcessor).end()

            .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200));
    }
    
    /**
     * Method to configure merge flight route
     */
    private void configureMergeFlightRoute() {
    	from("direct:mergeFlight")
        .onException(Exception.class).handled(true).process(errorProcessor).end()

        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204));
    }
}