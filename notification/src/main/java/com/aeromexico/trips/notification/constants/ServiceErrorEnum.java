package com.aeromexico.trips.notification.constants;

/**
 * WS API errors enum.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public enum ServiceErrorEnum {
	TC_FLIGHT_ERROR("TC_FLIGHT_ERROR", "600010"),
	TC_DEVICE_ERROR("TC_DEVICE_ERROR", "600020"),
	TC_NOTIFICATION_ERROR("TC_NOTIFICATION_ERROR", "600030"),
	TC_CP_LOAD_ERROR("TC_CPLOAD_ERROR", "600040");
	
	private String description;
	private String code;
	
	private ServiceErrorEnum(String description, String code) {
		this.description = description;
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public String getCode() {
		return code;
	}
}