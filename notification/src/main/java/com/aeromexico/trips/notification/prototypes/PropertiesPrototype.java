package com.aeromexico.trips.notification.prototypes;

import static com.aeromexico.trips.notification.constants.NotificationConstants.PROPERTIES_FILE;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Properties prototype class.
 * @author fjosorio | Aeromexico
 * @version 0.01
 */
public class PropertiesPrototype {
	/** Properties static object */
	private static Properties properties;
	
	/** Logger */
	private static Logger logger = LogManager.getLogger();
	
	static {
		init();
	}
	
	/**
	 * Init properties object
	 */
	private static void init() {
		properties = new Properties();
		try (InputStream is = new FileInputStream(PROPERTIES_FILE)) {
			properties.load(is);
			
		} catch (Exception e) {
			logger.fatal("Unable to load properties file: " + e.getMessage(), e);
		}
		
	}
	
	/**
	 * Method to get a property from properties object.
	 * @param key String. The anme of the property.
	 * @return String
	 */
	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
