package com.aeromexico.trips.notification.constants;

/**
 * The device status enum.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public enum DeviceStatusEnum {
	ACTIVE,
	NOT_ACTIVE;
}
