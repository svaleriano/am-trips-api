package com.aeromexico.trips.notification.processors;

import static com.aeromexico.trips.notification.constants.ServiceErrorEnum.TC_CP_LOAD_ERROR;

import org.apache.camel.Exchange;

/**
 * Club Premier services error processor.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class CPLoadErrorProcessor extends WSErrorProcessor {

	@Override
	public void process(Exchange exchange){
		super.buildError(exchange, TC_CP_LOAD_ERROR);
	}
	
}
