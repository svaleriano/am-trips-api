package com.aeromexico.trips.notification.processors;

import com.aeromexico.trips.notification.prototypes.SchemaValidationPrototype;

import static com.aeromexico.trips.notification.constants.NotificationConstants.SCHEMA_FILE_KEY;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.json.JSONObject;

/**
 * Generic JSON Schema Validation Processor 
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class SchemaValidationProcessor implements Processor {
	/** Logger object*/
	private static Logger logger = LogManager.getLogger();
	
	@Override
	public void process(Exchange exchange)
	throws Exception {
		String inBody = exchange.getIn().getBody(String.class);
		String schemaFile = exchange.getIn().getHeader(SCHEMA_FILE_KEY, String.class);
		Schema schema = SchemaValidationPrototype.getSchema(schemaFile);
		
		logger.info("AMTripNotification::SchemaValidationProcessor::File:"
        		+ schemaFile + "::Body: " + inBody);
		
		try{
            schema.validate(new JSONObject(inBody));
        } catch(ValidationException e){
            logger.error("AMTripNotification::SchemaValidationProcessor::Error: "
            		+ e.getMessage(), e);
            throw e;
        }
		
		exchange.getIn().setBody(inBody);
	}
}
