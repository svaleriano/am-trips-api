package com.aeromexico.trips.notification.constants;

/**
 * Camel registry context components enum.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public enum CamelComponentsEnum {
	JETTY_SERVLET_COMPONENT("jetty");
	
	private String name;
	
	private CamelComponentsEnum(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
}
