package com.aeromexico.trips.notification.constants;

/**
 * Trip Notification Constants
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class NotificationConstants {
	// GENERAL
	/** Project's data directory (templates and stuff).*/
	public static final String PROJECT_DATA_DIRECTORY = "/opt/software/bin/am-trips-api/config";
	
	// PROPERTIES
	/** Project's data directory (templates and stuff).*/
	public static final String PROPERTIES_FILE = PROJECT_DATA_DIRECTORY + "/properties/notification.properties";
	/** Rest host key*/
	public static final String REST_HOST_KEY = "rest.host";
	/** Rest host port*/
	public static final String REST_PORT_KEY = "rest.port";
	
	//SWAGGER
	/** Base context path for swagger and base route */
	public static final String SWAGGER_CONTEXT_PATH = "/am-trips-api";
	/** Description for swagger and base route */
	public static final String SWAGGER_DESCRIPTION = "Aeromexico Trips API";
	
	// SCHEMAS
	/** Add flight request json schema file.*/
	public static final String ADD_FLIGHT_RQ_SCHEMA_FILE = PROJECT_DATA_DIRECTORY + "/schemas/AddFlightRQSchema.json";
	/** Update device request json schema file.*/
	public static final String UPDATE_DEVICE_RQ_SCHEMA_FILE = PROJECT_DATA_DIRECTORY + "/schemas/UpdateDeviceRQSchema.json";
	/** Add device request json schema file.*/
	public static final String ADD_DEVICE_RQ_SCHEMA_FILE = PROJECT_DATA_DIRECTORY + "/schemas/AddDeviceRQSchema.json";
	/** JSON schema file header key.*/
	public static final String SCHEMA_FILE_KEY = "schemaFile";

	// HTTP ERRORS
	/** HTTP Client Error Code */
	public static final Integer HTTP_CLIENT_ERROR_CODE = 400;
	/** HTTP Server Error Code */
	public static final Integer HTTP_SERVER_ERROR_CODE = 500;
}
