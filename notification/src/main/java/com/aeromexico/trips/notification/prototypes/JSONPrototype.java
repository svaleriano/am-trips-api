package com.aeromexico.trips.notification.prototypes;

import com.google.gson.Gson;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Converter class for and to JSON.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class JSONPrototype {
	/** GSON objects */
    private static final Gson GSON;
    
   private JSONPrototype() {
	}

    static {
        GSON = new Gson();
    }

    /**
     * Method convert object to JSON string.
     * @param object Object. The object to be converted.
     * @return String
     */
    public static String convert(Object object) {
        return GSON.toJson(object);
    }

    /**
     * Method convert JSON string to class specific object.
     * @param clazz Class<T>. The class type of the object been converted.
     * @param json String. The json string to be converted.
     * @return T
     */
    public static <T> T convert(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }
    
    /**
     * Method convert JSON string to class specific list of objects.
     * @param type Type. The class type of the object been converted.
     * @param json String. The json string to be converted.
     * @return T
     */
    @SuppressWarnings("rawtypes")
	public static List convert(String json, Type type) {
        return GSON.fromJson(json, type);
    }
    
    /**
     * Method convert JSON string to class specific object.
     * @param clazz Class<T>. The class type of the object been converted.
     * @param is Reader. The input stream reader to be converted.
     * @return T
     */
    public static <T> T  convert(Reader is, Class<T> clazz) {        
        return GSON.fromJson(is, clazz);
    }
}

