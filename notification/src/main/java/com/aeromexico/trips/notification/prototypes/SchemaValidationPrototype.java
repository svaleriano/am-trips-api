package com.aeromexico.trips.notification.prototypes;

import static com.aeromexico.trips.notification.constants.NotificationConstants.ADD_FLIGHT_RQ_SCHEMA_FILE;
import static com.aeromexico.trips.notification.constants.NotificationConstants.ADD_DEVICE_RQ_SCHEMA_FILE;
import static com.aeromexico.trips.notification.constants.NotificationConstants.UPDATE_DEVICE_RQ_SCHEMA_FILE;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;

/**
 * Schema validation object prototype. 
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class SchemaValidationPrototype {
	/** JSON Schema Object */
	private static final Map<String, Schema> JSON_SCHEMAS = new HashMap<String, Schema>();
	/** Logger object */
	private static final Logger logger = LogManager.getLogger();
	
	private SchemaValidationPrototype() {
	}
	
	static {
		loadSchema(ADD_FLIGHT_RQ_SCHEMA_FILE);
		loadSchema(UPDATE_DEVICE_RQ_SCHEMA_FILE);
		loadSchema(ADD_DEVICE_RQ_SCHEMA_FILE);
	}
	
	/**
	 * Initializes json schema based on a file.
	 */
	private static void loadSchema(String schemaFile) {
		try (InputStream inputStream = new FileInputStream(schemaFile)) {
			byte []content = new byte[2048];
			inputStream.read(content);
			
			if (content.length == 0) {
				throw new IllegalArgumentException("Empty schema file: " + schemaFile);
			}
			
			SchemaLoader loader = SchemaLoader.builder()
					.schemaJson(new JSONObject(new String(content)))
					.draftV6Support()
					.build();
			
			JSON_SCHEMAS.put(schemaFile, loader.load().build());
		} catch (Exception e) {
			logger.fatal("AMTripNotification::SchemaValidationPrototype::Exception: "
			+ e.getMessage(), e);
		}
	}
	
	/**
	 * Method that delivers a new json schema.
	 * @param file String. The file of the schema (see {@link DocumentConstants}). 
	 * @return Schema.
	 */
	public static Schema getSchema(String file) {
		return JSON_SCHEMAS.get(file);
	}
}
