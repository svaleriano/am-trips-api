package com.aeromexico.trips.notification.constants;

/**
 * Enum to define a REST payload format - Using only the most popular formats
 * (JSON, XML AND TEXT) -
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public enum PayloadFormatsEnum {
    TEXT("text/plain"),
    XML("application/xml"),
    JSON("application/json");

    private String contentType;

    private PayloadFormatsEnum(String contentType) {
        this.contentType = contentType;
    }

    public String getContentType() {
        return this.contentType;
    }
}
