package com.aeromexico.trips.notification.processors;

import com.aeromexico.commons.model.ExtraInfo;
import com.aeromexico.commons.model.Incident;
import com.aeromexico.commons.web.types.ActionableType;
import com.aeromexico.trips.notification.constants.ServiceErrorEnum;
import com.aeromexico.trips.notification.prototypes.JSONPrototype;

import static com.aeromexico.trips.notification.constants.NotificationConstants.HTTP_CLIENT_ERROR_CODE;
import static com.aeromexico.trips.notification.constants.NotificationConstants.HTTP_SERVER_ERROR_CODE;
import static com.aeromexico.trips.notification.constants.PayloadFormatsEnum.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.everit.json.schema.ValidationException;
import org.json.JSONException;

/**
 * WS Error Processor Class to Handle HTTP Error Responses.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public abstract class WSErrorProcessor implements Processor {
	/** Logger class. */
	private static Logger logger = LogManager.getLogger();
	/**
     * Method to build an HTTP error response.
     * @param exchange {@link Exchange}
     * @param apiErrorCode {@link String}
     * @param apiErrorMessage {@link String}
     */
    protected void buildError(Exchange exchange, ServiceErrorEnum serviceError) {
    	Exception exception = getExceptionFromExchange(exchange);
    	Integer httpErrorCode = null;
        
    	if (exception instanceof ValidationException ||
    		exception instanceof JSONException) {
    		httpErrorCode = HTTP_CLIENT_ERROR_CODE;
    	} else {
    		httpErrorCode = HTTP_SERVER_ERROR_CODE;
    	}
        
    	exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE,
        		httpErrorCode);
        
        exchange.getOut().setHeader(Exchange.CONTENT_TYPE,
                JSON.getContentType());
        
        String bodyError= createBodyError(serviceError.getCode(),
        		serviceError.getDescription(),
        		exception.getMessage());
        
        logger.error("AMTripNotification::WSErrorProcessor::Body: {}", bodyError);  
        
        exchange.getOut().setBody(bodyError);
    }
    
    /**
     * Method to create a new API body error for HTTP response.
     * @param apiErrorCode {@link String}
     * @param apiErrorMessage {@link String}
     * @param debugStuff {@link String}
     * @return String
     */
    private String createBodyError(String apiErrorCode,
    		String apiErrorMessage,
    		String debugStuff) {
    	Incident incident = new Incident(apiErrorCode,
                ActionableType.FIX,
                apiErrorMessage,
                new ExtraInfo(debugStuff));
    	
    	return JSONPrototype.convert(incident);
    }

	/**
     * Method to get the route error (exception) from exchange.
     * @param exchange {@link Exchange}
     * @return {@link Exception}
     */
    private Exception getExceptionFromExchange(Exchange exchange) {
        Exception exception = null;

        // Get the exception (one of two possibilities).
        if (exchange.getException() != null) {
            exception = exchange.getException();
        } else if (exchange.getProperty(Exchange.EXCEPTION_CAUGHT) != null) {
            exception = (Exception) exchange.getProperty(Exchange.EXCEPTION_CAUGHT);
        }

        return exception;
    }

}
