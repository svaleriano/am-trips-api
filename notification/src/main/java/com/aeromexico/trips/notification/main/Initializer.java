package com.aeromexico.trips.notification.main;

import static com.aeromexico.trips.notification.constants.CamelComponentsEnum.JETTY_SERVLET_COMPONENT;

import org.apache.camel.CamelContext;
import org.apache.camel.component.jetty.JettyHttpComponent;
import org.apache.camel.component.jetty9.JettyHttpComponent9;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.JndiRegistry;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aeromexico.trips.notification.routes.DeviceRouteBuilder;
import com.aeromexico.trips.notification.routes.FlightRouteBuilder;
import com.aeromexico.trips.notification.routes.GeneralRouteBuilder;

/**
 * Initializer Class: Starts all Camel Routes and Components.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class Initializer {
	/** The context registry to save some additional instances */
	private static final JndiRegistry registry = new JndiRegistry(true);
	/** The context to add components and run all routes */
	private static final CamelContext context = new DefaultCamelContext(registry);
	
	private static final Logger logger = LogManager.getLogger();
	
	static {
            initServletComponent();
	}
	
	/**
	 * Method to initialize and add schema validation instances.
	 * @param registry JndiRegistry. The context registry to add instances.
	 */
	private static void initServletComponent() {
		JettyHttpComponent jettyComponent = new JettyHttpComponent9();
		
		context.addComponent(JETTY_SERVLET_COMPONENT.getName(),
				jettyComponent);
	}
	
	public static void main(String ... args) {
            
		try {
			context.addRoutes(new GeneralRouteBuilder());
			context.addRoutes(new DeviceRouteBuilder());
			context.addRoutes(new FlightRouteBuilder());
			
			context.start();
		} catch (Exception e) {
			logger.fatal("Error initializing am trips api instances: " + e.getMessage(), e);
		}
	}
}
