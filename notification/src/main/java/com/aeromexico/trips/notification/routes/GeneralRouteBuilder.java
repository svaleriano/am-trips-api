package com.aeromexico.trips.notification.routes;

import static com.aeromexico.trips.notification.constants.CamelComponentsEnum.JETTY_SERVLET_COMPONENT;
import static com.aeromexico.trips.notification.constants.NotificationConstants.REST_HOST_KEY;
import static com.aeromexico.trips.notification.constants.NotificationConstants.REST_PORT_KEY;
import static com.aeromexico.trips.notification.constants.NotificationConstants.SWAGGER_CONTEXT_PATH;
import static com.aeromexico.trips.notification.constants.NotificationConstants.SWAGGER_DESCRIPTION;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

import com.aeromexico.trips.notification.prototypes.PropertiesPrototype;

/**
 * General route builder (for all routes)
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class GeneralRouteBuilder extends RouteBuilder {
    
	@Override
	public void configure() throws Exception {
		errorHandler(noErrorHandler());
		
		buildRestConfiguration();
		buildLoadFlightsCron();
	}

	/**
	 * Method to build general rest configuration.
	 */
	private void buildRestConfiguration() {
		restConfiguration()
			.host(PropertiesPrototype.getProperty(REST_HOST_KEY))
			.port(PropertiesPrototype.getProperty(REST_PORT_KEY))
			.component(JETTY_SERVLET_COMPONENT.getName())
			.contextPath(SWAGGER_CONTEXT_PATH)
			.enableCORS(true)
			.bindingMode(RestBindingMode.off)
		
			.apiContextPath(SWAGGER_CONTEXT_PATH)
	        .apiProperty("title", SWAGGER_DESCRIPTION)
	        .apiProperty("version", "0.0.1");
	}

	/**
	 * Method to build general cron daemon which loads flights to trip db.
	 */
	private void buildLoadFlightsCron(){
		from("quartz2://loadFlights?cron=0+0/5+*+1/1+*+?+*")
		.onException(Exception.class).log(LoggingLevel.ERROR, "AMTripNotification::FlightsCron::Body: ${in.body} \n Headers: ${in.headers}").end()
			.log(LoggingLevel.INFO, "AMTripNotification::FlightsCron::LOADING FLIGHTS FROM SABRE");
		//TODO: Add something to request Sabre given each FFN and configure
        }
}
