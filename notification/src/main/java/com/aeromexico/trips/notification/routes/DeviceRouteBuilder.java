package com.aeromexico.trips.notification.routes;

import static com.aeromexico.trips.notification.constants.NotificationConstants.SCHEMA_FILE_KEY;
import static com.aeromexico.trips.notification.constants.NotificationConstants.ADD_DEVICE_RQ_SCHEMA_FILE;
import static com.aeromexico.trips.notification.constants.NotificationConstants.UPDATE_DEVICE_RQ_SCHEMA_FILE;
import static com.aeromexico.trips.notification.constants.PayloadFormatsEnum.JSON;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestParamType;

import com.aeromexico.trips.notification.processors.DeviceErrorProcessor;
import com.aeromexico.trips.notification.processors.SchemaValidationProcessor;
import com.aeromexico.trips.notification.processors.WSErrorProcessor;

/**
 * Device Routes
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class DeviceRouteBuilder extends RouteBuilder  {
	/** Device API Base Path */
	private static final String BASE_PATH = "/devices";
	/** Update Device Path */
	private static final String UPDATE_PATH = "${deviceId}/update";
	/** Add Device Path */
	private static final String ADD_PATH = "/add";
	/** Delete Device Path */
	private static final String DELETE_PATH = "${deviceId}/delete";
	
	/** Processor to validate json schema. */
	private static final SchemaValidationProcessor schemaValidationProcessor =
			new SchemaValidationProcessor();
	/** Processor to manage errors. */
	private static final WSErrorProcessor errorProcessor =
			new DeviceErrorProcessor();
	
	@Override
	public void configure()
	throws Exception {
        rest(BASE_PATH).description("AM Trip Central - Device Services")
        	
        	.put(UPDATE_PATH).description("Update Device Service")
        	.param().name("deviceId").type(RestParamType.path).description("The device identifier").required(true).endParam()
        	.consumes(JSON.getContentType())
        	.to("direct:updateDevice")
        	
        	.post(ADD_PATH).description("Add Device Service")
        	.param().name("deviceId").type(RestParamType.path).description("The device identifier").required(true).endParam()
        	.param().name("ffToken").type(RestParamType.header).description("The frequent flyer token").required(true).endParam()
        	.param().name("ffNumber").type(RestParamType.header).description("The frequent flyer number").required(true).endParam()
        	.consumes(JSON.getContentType())
        	.to("direct:addDevice")
        
    		.delete(DELETE_PATH).description("Delete Device Service")
    		.param().name("deviceId").type(RestParamType.path).description("The device identifier").required(true).endParam()
    		.param().name("ffToken").type(RestParamType.header).description("The frequent flyer token").required(false).endParam()
        	.param().name("ffNumber").type(RestParamType.header).description("The frequent flyer number").required(false).endParam()
    		.to("direct:deleteDevice");
        
        configureAddDeviceRoute();
        configureUpdateDeviceRoute();
        configureDeleteDeviceRoute();
	}
	
	/**
	 * Method to configure add device route.
	 */
	private void configureAddDeviceRoute() {
		from("direct:updateDevice")
		.onException(Exception.class).handled(true).process(errorProcessor).end()
			.setHeader(SCHEMA_FILE_KEY, constant(ADD_DEVICE_RQ_SCHEMA_FILE))
			.process(schemaValidationProcessor)
			
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(201));
	}
	
	/**
	 * Method to configure update device route.
	 */
	private void configureUpdateDeviceRoute() {
		from("direct:updateDevice")
		.onException(Exception.class).handled(true).process(errorProcessor).end()
			.setHeader(SCHEMA_FILE_KEY, constant(UPDATE_DEVICE_RQ_SCHEMA_FILE))
			.process(schemaValidationProcessor)
			
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204));
	}
	
	/**
	 * Method to configure delete device route.
	 */
	private void configureDeleteDeviceRoute() {
		from("direct:deleteDevice")
		.onException(Exception.class).handled(true).process(errorProcessor).end()
		
			.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(204));
	}
}
