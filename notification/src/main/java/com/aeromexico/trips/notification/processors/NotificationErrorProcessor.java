package com.aeromexico.trips.notification.processors;

import static com.aeromexico.trips.notification.constants.ServiceErrorEnum.TC_NOTIFICATION_ERROR;

import org.apache.camel.Exchange;

/**
 * Notification services error processor.
 * @author fjosorio | Aeromexico
 * @version 0.0.1
 */
public class NotificationErrorProcessor extends WSErrorProcessor {

	@Override
	public void process(Exchange exchange){
		super.buildError(exchange, TC_NOTIFICATION_ERROR);
	}
	
}
